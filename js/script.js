// Відповіді на питання:

// 1.	Описати своїми словами навіщо потрібні функції у програмуванні.
    // Функція в JS потрібна для того, щоб не дублювати одну й туж саму частину коду. Достатньо написати функцію один раз, а потім тільки викликати її та задавати їй аргументи, які вона буде використовувати

// 2.	Описати своїми словами, навіщо у функцію передавати аргумент.
    // Аргумент функції – це ті дані, які ми передаємо в функцію під час її виклику. Аргумент передається в параметри. 

// 3.	Що таке оператор return та як він працює всередині функції?
    // Оператор return потрібен, щоб повернути результат( чи будь-які інші дані) з функції назовні. Оператор return зупиняє виконання функції. Все що після нього виконуватись не буде. В функцію може бути будь яка кількість return.


// Завдання №1

let num1 = prompt('Введіть будь ласка перше число', '').trim();
while (
    isNaN(num1) ||
    num1 === undefined ||
    num1 === '' ||
    num1 === null ||
    num1 === " "
    ) {
    num1 = prompt('Будь ласка введіть саме число', `${num1}`).trim();  
}

let sign = prompt('Введіть будь ласка знак математичної операції, яку бажаєте виконати(додавання (+), віднімання (-), множення (*), ділення (/)', '').trim();
while (
    sign !== "+" &&
    sign !== "-" &&
    sign !== "*" &&
    sign !== "/") {
    sign = prompt('Будь ласка введіть арифметичний знак вказаний в модальному вікні', `${sign}`).trim();  
}

let num2 = prompt('Введіть будь ласка друге число', '').trim();
while (
    isNaN(num2) ||
    num2 === undefined ||
    num2 === '' ||
    num2 === null ||
    num2 === " ") {
    num2 = prompt('Будь ласка введіть саме число', `${num2}`).trim();  
}

// Викнання через switch

function arithmetic (firstNum, secondNum, signFunc) {
    switch(signFunc) {
        case '+': 
        return (+firstNum) + (+secondNum);
        case '-': 
        return (+firstNum) - (+secondNum);
        case '*': 
        return (+firstNum) * (+secondNum);
        case '/': 
        return (+firstNum) / (+secondNum);
    }
}
console.log(arithmetic(num1, num2, sign));

// Виконання через if

// function arithmetic(firstNum, secondNum, signFunc) {
//     if( signFunc === "+") {
//         return (+firstNum) + (+secondNum);
//     } else if ( signFunc === '-') {
//         return (+firstNum) - (+secondNum);
//     } else if ( signFunc === '*') {
//         return (+firstNum) * (+secondNum);
//     }else if ( signFunc === '/') {
//         return (+firstNum) / (+secondNum);
//     }
// }
// console.log(arithmetic(num1, num2, sign));